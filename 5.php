<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
       
        <?php
        function calculoColor(){
            $color = "rgb(" . rand (0,255) . "," . rand (0,255) . "," . rand (0,255) . ")";
            return $color;
        }
        
        $color = calculoColor();
        ?>
        
        <p> Color: <?= $color ?> </p>
        <div style="width:50px;height:50px;border-radius:50px;background-color:<?= calculoColor() ?>"> </div>
    </body>
</html>
