<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <script type="text/javascript">
    
    window.addEventListener("load",arranca);
    
    function arranca(){
    var linea=document.querySelector("line");
        linea.addEventListener("click",salta);
    }
    function salta(event){
        var valor;
        valor=Math.random() * (100-1) +1;
        event.target.setAttribute("x2",valor);
    }
    </script>
    <body>
       
        <?php
            $longitud = rand(1,100);
            
            print " <p> Longitud: $longitud </p>";
            echo "<br>";
        ?>
        
        <svg width="<?=$longitud?>px" height="10px">
        
        <?php
            echo '<line x1="1" y1="5" x2="' . $longitud . '" y2="5" stroke="red" stroke-width="10" >';
        ?>
        
        </svg>
        
    </body>
</html>
