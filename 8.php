<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script>
        window.addEventListener("load",()=>{
        var circulo=document.querySelector("circle");
            circulo.addEventListener("click",() => {
                    circulo.style.display="none";
            });
        });
        </script>
    </head>
    <body>
       
        <?php
        function calculoColor(){
            $color = "rgb(" . rand (0,255) . "," . rand (0,255) . "," . rand (0,255) . ")";
            return $color;
        }
        $color = calculoColor();
        
        function dibujarCirculo($x,$y){
             echo '<circle cx="'.$x.'" cy="'.$y.'" r="50" fill="'.calculoColor().'" />';
        }
        
        
        ?>
        
        <p> Color: <?= $color ?> </p>
        <svg width="100px" height="100px">
       <?php 
       dibujarCirculo(50,50)
       ?>
        </svg>
        
    </body>
</html>
