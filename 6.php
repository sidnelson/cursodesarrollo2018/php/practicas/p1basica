<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
       
        <?php
        function calculoColor(){
            $color = "rgb(" . rand (0,255) . "," . rand (0,255) . "," . rand (0,255) . ")";
            return $color;
        }
        
        $color = calculoColor();
        ?>
        
        <p> Color: <?= $color ?> </p>
        <svg width="50px" height="50px">
       <?php 
       echo '<circle cx="25" cy="25" r="25" fill="'. calculoColor() . '" />';
       ?> 
        </svg>
        
    </body>
</html>
